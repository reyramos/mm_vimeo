###mm_vimeo
==========================================


Wordpress plugin which utilizes vimeo advance api to gather all public video from the user collection.

##Requirements

- Vimeo Advance API client id and secret, found in the OAuth page.<https://developer.vimeo.com/apis>

##Documentation

Once the mm_vimeo has been activate it will add a new page onto the wordpress pages which would be used to make the RESTAPI call.
This page can then be used to make daily cron job to update your vimeo library within wordpress.

- New page would be "vimeo.videos.getall", it is recommended to use Post name permalink found in wordpress settings page, the script works in both default and post name settings.

- Upon activation the page will redirect to Tools => Vimeo oAuth settings. If these settings are not set the RESTful page mention above will provide SERVER ERROR 500, invalid consumer key.

Once all necessary data has been set, congratulation you may view your new page which will load the first page of the vimeo call.
The page will only load the first top 50 videos, for any additional page execute the following script after updating the sites BASE_URL, 'vimeo.videos.getAll.sh'

The script will call onto the new RESTful site and redirect to the next page if there are more videos to load.
