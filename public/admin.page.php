<div class="mm-vimeo-wrapper">
	<div class="row">
		<div class=" col-sm-12 col-md-10 col-lg-6">
			<h2><?php echo $header?></h2>
			<form method="post" action="">
				<?php
				//array of form fields
				$args = array('mm-video-client_id','mm-video-client_secret','mm-video-access_token','mm-video-token_secret','mm-video-user_id');
				$count = 1;
				foreach($args as $arg){
					if(validate($arg)){
						add_option( $arg, $_POST[$arg] )?true:update_option( $arg, $_POST[$arg] );
						$count++;
						if(sizeof($args) == $count){
							echo '<div class="alert alert-success">'.$SUCCESS.'</div>';
						}
					}
				}

				function validate($post){
					if(isset($_POST[$post]) && empty($_POST[$post]))delete_option( $post );
					return isset($_POST[$post]) && !empty($_POST[$post])?true:false;
				}

				?>

				<div class="form-group <?php echo !validate('mm-video-client_id') && validate('submit')? "has-error has-feedback":"";  ?> ">
					<label for="clientId"><?php echo $clientId?></label>
					<input type="text" class="form-control" id="clientId"  name="mm-video-client_id" value="<?php echo get_option('mm-video-client_id'); ?>">
					<?php if (!validate('mm-video-client_id') && validate('submit')):?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
					<?php endif;
					echo $cIBlock;
					?>
				</div>
				<div class="form-group <?php echo !validate('mm-video-client_secret') && validate('submit')? "has-error has-feedback":"";  ?> ">
					<label for="clientSecret"><?php echo $clientSecret; ?></label>
					<input type="text" class="form-control" id="clientSecret"  name="mm-video-client_secret" value="<?php echo get_option('mm-video-client_secret'); ?>">
					<?php if (!validate('mm-video-client_secret') && validate('submit')):?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
					<?php endif;
					echo $cSBlock;
					?>
				</div>

				<h2><?php echo $THeader?></h2>
				<?php echo $TBlock?>
				<div class="form-group <?php echo !validate('mm-video-access_token') && validate('submit')? "has-error has-feedback":"";  ?> ">
					<label for="accessToken"><?php echo $accessToken; ?></label>
					<input type="text" class="form-control" id="accessToken"  name="mm-video-access_token" value="<?php echo get_option('mm-video-access_token'); ?>">
					<?php if (!validate('mm-video-access_token') && validate('submit')):?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
					<?php endif; ?>
				</div>
				<div class="form-group <?php echo !validate('mm-video-token_secret') && validate('submit')? "has-error has-feedback":"";  ?> ">
					<label for="accessSecret"><?php echo $accessSecret; ?></label>
					<input type="text" class="form-control" id="accessSecret"  name="mm-video-token_secret" value="<?php echo get_option('mm-video-token_secret'); ?>">
					<?php if (!validate('mm-video-token_secret') && validate('submit')):?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
					<?php endif; ?>
				</div>
				<h2><?php echo $UIHeader?></h2>
				<?php echo $UIBlock; ?>
				<div class="form-group">
					<label for="userId">User Id</label>
					<input type="text" class="form-control" id="userId"  name="mm-video-user_id" value="<?php echo get_option('mm-video-user_id'); ?>">
				</div>


				<?php submit_button(); ?>
			</form>
		</div>

	</div>

</div>