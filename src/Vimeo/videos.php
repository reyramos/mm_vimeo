<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 6/19/14
 * Time: 1:21 PM
 */


namespace MonsterMedia\Vimeo;

/**
 *
 * Gets all the videos from vimeo api,  vimeo.videos.getAll
 *
 * Class getVideos
 * @package MonsterMedia
 */
class videos
{
	protected $config;
	protected $vimeo;
	protected $page = 1;
	protected $jsonRequest;

	private $summary = array();
	private $videos = array();


	public function __construct()
	{
		global $json_api;

//		$this->jsonRequest = json_decode(stream_get_contents(fopen('php://input', 'r')), true);
		$this->config = array(
			'client_id' => get_option('mm-video-client_id'),
			'client_secret' => get_option('mm-video-client_secret'),
			'user_id' => get_option('mm-video-user_id') ? get_option('mm-video-user_id') : "me",
		);

		if(get_option('mm-video-client_id') && get_option('mm-video-client_secret')){
			$this->vimeo = new \phpVimeo($this->config['client_id'], $this->config['client_secret']);
		}else{
			$json_api->error("Missing Vimeo oAuth.");
		}
	}


	public function setPage($page)
	{
		$this->page = $page;
	}

	public function getAll()
	{
		$ceil = null;
		$generated_in = 0;
		$videosResult = null;

		$videos = $this->vimeo->call('vimeo.videos.getAll', array('user_id' => $this->config['user_id'], 'page' => $this->page, 'full_response' => true));
		$array = json_decode(json_encode($videos), true);
		$generated_in += floatval($array['generated_in']);
		$total = $array['videos']['total'];

		$this->videos = array_merge($this->videos, $array['videos']);
		$this->summary = array(
			'generated_in' => $generated_in,
			'total' => $total,
		);

		return $this->videos;
	}

	public function getSummary()
	{
		return $this->summary;
	}


}






