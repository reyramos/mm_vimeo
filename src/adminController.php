<?php

namespace MonsterMedia;

class adminController
{
	public function __construct(){}

	/**
	 * Displays the admin page
	 *
	 * @access public
	 * @return void
	 */
	function show_admin_page()
	{
		$strings = array(
			'header' => __('Vimeo oAuth'),
			'clientId' => __('Client Id*'),
			'cIBlock' => __('<p class="help-block">Also known as Consumer Key or API Key</p>'),
			'clientSecret' => __('Client Secret*'),
			'cSBlock' => __('<p class="help-block">Also known as Consumer Secret or API Secret</p>'),
			'THeader' => __('Your access token'),
			'TBlock' => __('<p class="help-block">You can use this OAuth Access Token to access your account with this app.</p>'),
			'accessToken' => __('Access Token*'),
			'accessSecret' => __('Access Token Secret*'),
			'UIHeader' => __('User ID'),
			'UIBlock' => __('<p class="help-block">This is optional, it will collect all information provided for the selected user, default is me.</p><p class="help-block">The ID number or
			username of the user. A token may be used instead.</p>'),
			'SUCCESS' => __('<strong>Well done!</strong> You successfully you\'ve added a new Vimoe oAuth.')
		);

		Render::render('admin.page.php', $strings);
	}


	/**
	 * Fires actions on option page load
	 *
	 * @return void
	 */
	function option_page_actions()
	{
		add_action('admin_enqueue_scripts', array($this, 'register_plugin_styles'));
		$this->_add_help_screen();
	}

	/**
	 * Adds any plugin styles to our page
	 *
	 * @access public
	 * @return void
	 */
	function register_plugin_styles()
	{
		wp_register_style("MM_VIDEO_BOOTSTRAP", MM_VIDEO_PLUGIN_URL . "/public/css/bootstrap.css", 'all');
		wp_enqueue_style('MM_VIDEO_BOOTSTRAP');
	}


	/**
	 * Adds v3.3 style help menu for plugin page
	 *
	 * @access private
	 * @return void
	 */
	function _add_help_screen()
	{
		get_current_screen()->add_help_tab(array(
			'id' => 'overview',
			'title' => __('Overview'),
			'content' =>
				'<p>' . __('This plugin allows you to set your vimeo developers oAuth security credentials.  With these credentials the plugin will be allow to call on selected vimeo advance API.
				 To view a list of the plugin RESTful api route to <code>Pages -> All Pages</code>.') . '</p>'
		));
		get_current_screen()->add_help_tab(array(
			'id' => 'instructions',
			'title' => __('Instructions'),
			'content' =>
				'<p>' . __('Enter all the information required.') . '</p>' .
				'<p>' . __('Enter your Client Id and Client Secret, which it would be used to make the advance API call to vimeo') . '</p>' .
				'<p>' . __('Enter the User Id, default is <code>me</code>, the id can be either a number or the username of the users, a token may be used.') . '</p>'.
				'<p>' . __('Once you have successfully enter all the information needed, you can click on the new API page created within <code>Pages -> All Pages</code>') . '</p>'
		));

		get_current_screen()->set_help_sidebar(
			'<p><strong>' . __('Contact information:') . '</strong></p>' .
			'<p>' . __('') . '</p>'
		);
	}


} 