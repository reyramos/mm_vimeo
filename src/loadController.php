<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 6/30/14
 * Time: 10:43 AM
 */

namespace MonsterMedia;


class loadController
{

	private $table_name;
	private $category;

	function __construct()
	{
	}

	public function load_data($videos)
	{
		global $wpdb;
		$this->table_name = $wpdb->prefix . "vimeo_video";
		$this->category = get_cat_ID("vimeo");

		foreach ($videos as $key => $arr) {

			$result = $wpdb->get_row("SELECT ID, vid FROM " . $this->table_name . " WHERE vid = " . $arr['id']);
			if ($result) {
				//update the post if exist
				$arr = array_merge($arr, array("ID" => $result->{'ID'}));
				$this->insert_into_post($arr);
			} else {

				$vimeo_videos = array(
					'ID' => $this->insert_into_post($arr),
					'vid' => $arr['id'],
					'upload_date' => $arr['upload_date'],
					'width' => $arr['width'],
					'height' => $arr['height'],
					'duration' => $arr['duration'],
					'owner' => serialize($arr['owner']),
					'urls' => serialize($arr['urls']['url']),
					'thumbnails' => serialize($arr['thumbnails']['thumbnail'])
				);

				$wpdb->insert($this->table_name, $vimeo_videos);
			}
		}

	}


	/**
	 * function that will insert the vimeo api result into posts table,
	 * to be used during search parameters
	 * @param $arr
	 * @return int|\WP_Error
	 */
	public function insert_into_post($arr)
	{
		$arg = array(
			'post_content' => $arr['description'], // The full text of the post.
			'post_name' => strtolower(str_replace(" ", "_", $arr['title'])),
			'post_title' => $arr['title'], // The title of your post.
			'post_status' => "publish",
			'post_type' => "vimeo_video",
			'post_date' => date('Y-m-d H:i:s'),
			'comment_status' => "closed",
			'post_category' => array($this->category),
			'tags_input' => array('video')
		);

		if (isset($arr['ID'])) {
			$arg = array_merge($arg, array('ID' => $arr['ID']));
		}

		return wp_insert_post($arg);

	}


} 