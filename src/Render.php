<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 7/3/14
 * Time: 11:20 AM
 */

namespace MonsterMedia;


class Render
{

	private static $public_path = null;

	public static function path($path)
	{
		self::$public_path = plugin_dir_path(__DIR__) .rtrim($path, "/").'/';
	}

	public static function render($file, $param = array())
	{
		if (self::$public_path == null) {
			self::$public_path = plugin_dir_path(__DIR__) . 'public/';
		}

		foreach ($param as $k => $var) $$k = $var;

		if (is_dir(self::$public_path)) {
			include_once(self::$public_path . $file);
		} else {
			throw new \Exception('Missing path folder within root of plugin: '.self::$public_path);
		}

	}
}