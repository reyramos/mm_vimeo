<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 6/24/14
 * Time: 2:48 PM
 */

namespace MonsterMedia;

use MonsterMedia\Vimeo\getVideos;

class initController
{
	protected $table_name;
	protected $adminController;
	public $active = false;

	public function __construct()
	{
		if (!is_plugin_active('json-api/json-api.php')) {
			add_action('admin_head', array($this, '_draw_notice_json_api'));
		} else {
			$this->adminController = new adminController();
			add_action('admin_init', array($this, 'init'));
			add_action('admin_menu', array(&$this, 'add_admin_menu'));
			$this->active = true;
		}
	}

	/**
	 * Alert the admin page of missing plugin, it will remove activated plugin from database
	 */
	function _draw_notice_json_api()
	{

		echo '<div id="bbPress_message" class="error fade"><p style="line-height: 150%">';
		_e('<strong>MM Vimeo</strong> requires the JSON API plugin to be activated. Please <a href="http://wordpress.org/plugins/json-api/">install / activate JSON API</a> first,
			or <a href="plugins.php">deactivate MM Vimeo extension</a>.');
		echo '</p></div>';

		$this->_set_inactive();
		$this->_destroy_activation();
	}

	/**
	 * Hack to modify the view of the plugin page if plugin is not activated
	 */
	private function _set_inactive()
	{
		echo '<script>
			window.onload = function (){
				document.getElementById("monster-media-vimeo").classList.remove ("active");
				document.getElementById("monster-media-vimeo").classList.add ("inactive");
			}
		</script>';

		add_filter('plugin_action_links_mm_vimeo/monster_vimeo.php', array($this, '_set_action_buttons'));
	}


	function _set_action_buttons($links)
	{
		unset($links['deactivate']);

		$mylinks = array(
			'activate' => 'Activate',
			'edit' => 'Edit',
			'z' => 'Delete'
		);

		$newArr = array_merge($links, $mylinks);
		ksort($newArr);
		return $newArr;
	}

	private function _destroy_activation()
	{
		global $wpdb;
		//first check if it already exist
		$result = $wpdb->get_row("SELECT option_value FROM " . $wpdb->prefix . "options  WHERE option_name = 'active_plugins'");
		$unserialize = unserialize($result->option_value);
		foreach ($unserialize as $key => $string) {
			if (strcasecmp($string, 'mm_vimeo/monster_vimeo.php') == 0) {
				$index = $key;
			}
		}
		if (isset($index)) {
			unset($unserialize[$index]);
			$serialize = serialize($unserialize);
			$wpdb->update($wpdb->prefix . "options", array('option_value' => $serialize), array('option_name' => 'active_plugins'), array('%s'));
		}
	}

	/**
	 * Redirects the user after the plugin is activated
	 *
	 * @access private
	 * @return void
	 */
	function _redirect_user()
	{
		wp_redirect(admin_url('options-general.php') . '?page=mm-vimeo');
	}

	/**
	 * For activation hook
	 *
	 * @access public
	 * @return void
	 */
	function plugin_activate()
	{
		add_option(MM_VIDEO_PLUGIN_MD5 . '-mm-vimeo', true);
	}

	/**
	 *
	 * Create the prefix_vimeo_video table
	 * Sets a new category,
	 * Sets a template page
	 * Redirect user to create config file
	 *
	 */
	function init()
	{
		if (current_user_can('manage_options') && get_option(MM_VIDEO_PLUGIN_MD5 . '-mm-vimeo', false)):
			delete_option(MM_VIDEO_PLUGIN_MD5 . '-mm-vimeo');

			global $wpdb;
			$this->table_name = $wpdb->prefix . "vimeo_video";

			$sql = "CREATE TABLE IF NOT EXISTS " . $this->table_name . " (
  ID int(11) NOT NULL,
  vid int(11) NOT NULL,
  upload_date text NOT NULL,
  width int(11) NOT NULL,
  height int(11) NOT NULL,
  duration int(11) NOT NULL,
  owner text NOT NULL,
  urls text NOT NULL,
  thumbnails text NOT NULL,
  UNIQUE KEY (ID),
  UNIQUE KEY (vid)
   );";

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sql);

			$this->create_vimeo_cat();
			$this->_redirect_user();

		endif;
	}

	/**
	 * Create category
	 * @return void
	 */
	private function create_vimeo_cat()
	{
		$cat = array(
			'cat_name' => "Vimeo",
			'category_description' => __("a category to tag all post, inserted automatically by plugin"),
			'category_nicename' => 'vimeo',
			'taxonomy' => 'category'
		);

		wp_insert_category($cat, false);
	}

	/**
	 * Adds our submenu item to the Tools menu
	 *
	 * @access public
	 * @return void
	 */
	public function add_admin_menu()
	{
		if (current_user_can('manage_options')) {
			if (current_user_can('update_core')) {
				$this->_hook = add_options_page('Vimeo OAuth Settings', 'Vimeo OAuth', 'update_core', 'mm-vimeo', array($this->adminController, 'show_admin_page'));
				add_action('load-' . $this->_hook, array($this->adminController, 'option_page_actions'));
			} else {
				wp_die(__('You do not have sufficient permissions to access this page.'));
			}
		}
	}

}