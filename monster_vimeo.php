<?php
/*
 * Plugin Name: Monster Media Vimeo
 * Plugin URI:
 * Description: A plugin that uses vimeo advance api, requires for JSON API to be installed and activated.
 * Version: 1.0
 * Author: Creative Team
 * Author URI:
 * License: GPL2
 * */

if (!defined('ENVIROMENT')) {
	define('ENVIROMENT', (isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] == 'development') ? 'development' : 'production');
}

if (ENVIROMENT == 'development') {
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}

include_once(ABSPATH . 'wp-admin/includes/plugin.php');

define('MM_VIDEO_PLUGIN_MD5', '_e5e164f8c704b53cf30d738ca83000aa');
define('MM_VIDEO_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('MM_VIDEO_PLUGIN_URL', plugins_url("mm_vimeo"));

//autoload for vendors with namespaces
include_once MM_VIDEO_PLUGIN_PATH . 'vendor/autoload.php';
include_once MM_VIDEO_PLUGIN_PATH . "vendor/vimeo/vimeo-php-lib/vimeo.php";

// Create the database on install
$monster_media = new MonsterMedia\initController();
if ($monster_media->active) {
	register_activation_hook(__FILE__, array($monster_media, 'plugin_activate'));
}


if (is_plugin_active('json-api/json-api.php')) {
	add_filter('json_api_controllers', '_e5e164f8c704b53cf30d738ca83000aa_mm_vimeo_controller');
	add_filter('json_api_mmvimeo_controller_path', '_e5e164f8c704b53cf30d738ca83000aa_mm_vimeo_controller_path');
}

function _e5e164f8c704b53cf30d738ca83000aa_mm_vimeo_controller($controllers)
{
	$controllers[] = 'MMVimeo';
	return $controllers;
}


function _e5e164f8c704b53cf30d738ca83000aa_mm_vimeo_controller_path()
{
	return MM_VIDEO_PLUGIN_PATH . 'controllers/mmvimeo.php';
}


function _e5e164f8c704b53cf30d738ca83000aa_create_post_type()
{
	$labels = array(
		'name' => 'Vimeo Videos',
		'singular_name' => 'Vimeo Video',
		'add_new' => false
	);
	$args = array(
		'label' => 'vimeo',
		'description' => 'All video from vimeo',
		'labels' => $labels,
		'taxonomies' => array('category'),
		'public' => true,
		'query_var' => true,
		'has_archive' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'capabilities' => array('read_post')
	);

	register_post_type('vimeo_video', $args);

}

add_action('init', '_e5e164f8c704b53cf30d738ca83000aa_create_post_type');

/**
 * @param array $params
 * @return string|WP_Error
 */
function get_vimeo_post($params = array())
{

	$params = array_merge(array('iframe-class' => 'vimeo-iframe', 'echo' => true, 'iframe-id' => ''), $params);

	if (!isset($params['post_id']) || empty($params['post_id'])) {
		return new WP_Error('Missing', __("missing post_id"));
	}

	global $wpdb;
	$result = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "vimeo_video  WHERE ID = '" . $params['post_id'] . "'");

	$iframe = "<iframe class='" .
		$params['iframe-class'] . "' " .
		(array_key_exists('iframe-id', $params) && !empty($params['iframe-id']) ? $params['iframe-id'] : "") . " src='http://player.vimeo.com/video/" .
		$result->{'vid'} . "?title=0&byline=0&portrait=0&api=1' width='" .
		$result->{'width'} . "' height='" .
		$result->{'height'} . "'></iframe>";

	if ($params['echo']) {
		echo $iframe;
	} else {
		return $iframe;
	}
}

