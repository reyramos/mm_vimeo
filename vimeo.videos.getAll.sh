#!/bin/bash
BASE_URL="http://redroger-ubuntu/wordpress/api/mmvimeo/get_videos/?page="

# Script to run on CRON to update the _vimeo_video table, change the url to RESTAPI site found in wp all pages,
# Script will redirect to next page if there is more vimeo videos than the page display
#
# Must provide ?page= within the URL of your REST API url
# BASE_URL="http://wordpress-site/api/mmvimeo/get_videos/?page=" DO NOT ADD THE LEADING NUMBER

############################################# DO NOT CHANGE #############################################
# start of with the first page
PAGE=1
# set url for lookup
URL="${BASE_URL}${PAGE}"

function curlCommand(){
	JSON=$(curl -Ls "${URL}" | php -r 'echo file_get_contents("php://stdin");');
	PAGE=$(echo "$JSON"|php -r 'echo json_decode(file_get_contents("php://stdin"))->page;');
	TOTAL=$(echo "$JSON"|php -r 'echo json_decode(file_get_contents("php://stdin"))->total;');
	PERPAGE=$(echo "$JSON"|php -r 'echo json_decode(file_get_contents("php://stdin"))->perpage;');
	CALLS=$(printf "%.0f" $(echo "scale=2;$TOTAL/$PERPAGE" | bc))
	(( PAGE++ ))
	URL="${BASE_URL}${PAGE}"
	return $TRUE;
};


c=1
CALLS=1

while [ "${c}" -le "${CALLS}"  ]
do
		curlCommand
		STRING='+'
		RATIO=$(printf "%.0f" $(echo "scale=2;100/$CALLS" | bc))
		PERCENT=$(printf "%.0f" $(echo "$RATIO * $c" | bc))
		OFFSET=$(printf "%.0f" $(echo "scale=2;100 - $PERCENT" | bc))
		for ((i=2; i<=PERCENT; ++i )) ;
		do
			STRING+='+'
		done
		for ((k=2; k<=OFFSET; ++k ));
		do
			STRING+=' '
		done
		echo  -ne ''"$STRING"'('"$PERCENT"'%)\r'
		(( c++ ))
done
		echo -ne '\n'
		echo $(echo "$JSON"|php -r 'echo json_decode(file_get_contents("php://stdin"))->total;') "videos loaded ...";

