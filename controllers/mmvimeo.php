<?php
/*
Controller name: MM Vimeo
Controller description: Monster Media DOOH Vimeo.video.api, Controller based for Vimeo advance api
*/
use MonsterMedia\Vimeo\videos;

class JSON_API_MMVimeo_Controller
{
	public function get_videos()
	{
		global $json_api;
		$page = $json_api->query->page;

		$videos = new videos();

		if (isset($page)) {
			$videos->setPage($page);
		}

		$videos = $videos->getAll();

		$load = new \MonsterMedia\loadController();
		$load->load_data($videos['video']);

		return $videos;
	}

}
